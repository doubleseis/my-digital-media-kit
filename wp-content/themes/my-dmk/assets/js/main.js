$(document).ready(function() {

    $(".toggle").click(function() {
       $('.links').slideToggle();
    });
    $(window).resize(function() {
       if ($(window).width() > 768) {
          $('.links').show();
       } else {
          $('.links').hide();
       }
    });

    $('.testimonials__carousel').owlCarousel({
      loop:true,
      margin:0,
      nav:true,
      dots:false,
      items:1,
      navText:['<','>']
    });

    $('a[href*="#"]:not([href="#"]):not([href="#show"]):not([href="#hide"])').click(function() {
    	if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
    		var target = $(this.hash);
    		target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
    		if (target.length) {
    			$('html,body').animate({
    				scrollTop: target.offset().top
    			}, 1000);
    			return false;
    		}
    	}
    });

});
