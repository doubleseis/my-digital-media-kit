<!doctype html>
<html>

<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

<title><?php bloginfo('name'); ?> <?php wp_title(); ?></title>

<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=0">

<?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>

<header class="container-fluid main-header">
  <div class="container">
    <section class="row">
      <div class="col-lg-3 logo text-center">
        <div class="logo__container">
          <a href="<?php echo home_url(); ?>">
            <img class="logo--mydmk" src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/mydmk-logo.png" alt="">
          </a>
        </div>
      </div>
      <div class="col-lg-9 main-menu">
        <nav class="navbar navbar-expand-lg">
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <i class="fas fa-bars"></i>
          </button>
          <div class="collapse navbar-collapse" id="navbarNav">
            <?php wp_nav_menu(array('menu'=>'Primary Menu', 'menu_class' => 'nav navbar-nav' )); ?>
          </div>
        </nav>
      </div>
    </section>
  </div>
</header>
