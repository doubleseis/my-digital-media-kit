<footer class="text-center">
  <div class="container-fluid">
    <?php wp_nav_menu(array('menu'=>'Primary Menu', 'menu_class' => 'nav navbar-nav' )); ?>
    <p class="copyright text-center">&copy; 2003-<?php echo date('Y'); ?> <a href="<?php echo home_url(); ?>">LINDSAY DICKS</a> | All Rights Reserved </p>
  </div>
</footer>

<?php wp_footer(); ?>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Membership to the Impact Speakers Group</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>The Impact Speakers Group is a community of thousands of Best-Selling Authors® and Celebrity Experts® in various fields that we’re able to match with event planners and booking agents looking for speakers with your specific knowledge</p>
        <p><a href="http://impactspeakersgroup.com/" target="_blank" style="color:blue;">Click Here to Learn More</a></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

</body>
</html>
