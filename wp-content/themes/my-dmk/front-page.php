<?php get_header(); ?>

<div class="hero">
  <div class="container">
    <div class="row">
      <div class="col-lg-6">
        <h2>Your Media<br>All in One Place</h2>
        <p>Whether you’re looking for a hub for all your media credibility, to expand your Digital DNA<sup>&trade;</sup> or to showcase your credibility online our Digital Media Kits are right for YOU!</p>
        <a href="#" class="btn btn-default">Sign Up Now!</a>
      </div>
    </div>
  </div>
</div>

<div id="features" class="feature__container">
  <div class="container">
    <div class="row">
      <div class="feature__heading text-center col">
        <h3>Features</h3>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-4 text-center">
        <div class="feature">
          <div class="feature__icon">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/feature--1.png" alt="">
          </div>
          <div class="feature__content">
            <h5>Showcase Your Credibility</h5>
            <p>Display your credentials all in one place for prospects and journalists.</p>
          </div>
        </div>
      </div>

      <div class="col-lg-4 text-center">
        <div class="feature">
          <div class="feature__icon">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/feature--2.png" alt="">
          </div>
          <div class="feature__content">
            <h5>Personalize It</h5>
            <p>Personalized just for your <br>needs</p>
          </div>
        </div>
      </div>

      <div class="col-lg-4 text-center">
        <div class="feature">
          <div class="feature__icon">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/feature--3.png" alt="">
          </div>
          <div class="feature__content">
            <h5>Clean and Professional</h5>
            <p>No clutter, no fuss. Just clean, professional and down to the point- you’re credible!</p>
          </div>
        </div>
      </div>

    </div>
  </div>
</div>

<div id="demo" class="demo">
  <div class="container">
    <div class="col-md-6 offset-md-3 demo__btns">
      <a href="http://lindsaydicks.mydigitalmediakit.com/" target="_blank" class="btn btn__demo btn__demo--left"><img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/icon-play.png"> View Live Demo</a>
      <a href="https://dicksnanton.infusionsoft.com/app/manageCart/addProduct?productId=3005" target="_blank" class="btn btn__demo btn__demo--right"><img src="<?php echo get_stylesheet_directory_uri(); ?>/dist/images/icon-sign.png"> Sign Up Now</a>
    </div>
  </div>
</div>


<div id="pricing" class="container package__container">
  <div class="row">
    <div class="feature__heading text-center col">
      <h3>Packages</h3>
    </div>
  </div>
  <div class="row">
    <div class="col-lg-12 text-center">

      <div class="package package__professional">
        <div class="package__price">
          <p>Digital Media Kit</p>
          <p><small><sup>$</sup>397 sign-up and then</small></p>
          <big><sup>$</sup>29.95/mo</big>
        </div>
        <div class="package__description">
          <ul>
            <li>Amplify your message and build your brand with the complete Digital Media Kit</li>
            <li>Personalized Media Kit button for your website to make it easily accessible from your current website</li>
            <li><a href="https://dicksnanton.infusionsoft.com/app/manageCart/addProduct?productId=3005" target="_blank" class="btn btn__contrast">Purchase Now</a></li>
          </ul>
        </div>
      </div>

      <div class="package package__business">
        <div class="package__price">
          <p>Digital Media Kit PLUS Impact Speakers Group Membership!</p>
          <p><small><sup>$</sup>997 sign-up and then</small></p>
          <big><sup>$</sup>47/mo</big>
        </div>
        <div class="package__description">
          <ul>
            <li>Amplify your message and build your brand with the complete Digital Media Kit</li>
            <li>Personalized Media Kit button for your website to make it easily accessible from your current website</li>
            <li>Membership to the Impact Speakers Group <sup><strong><a href="#" data-toggle="modal" data-target="#exampleModal" style="text-decoration:underline;color:blue;">?</a></strong></sup></li>
            <li>Be featured on the Impact Speakers Group Website</li>
            <li>Receive a a Press Release on becoming part of the Impact Speakers Group</li>
            <li>Personalized Official Membership Certificate</li>
            <li><a href="https://dicksnanton.infusionsoft.com/app/orderForms/CelebritySites-Impact-Speakers-Group" target="_blank" class="btn btn__contrast">Purchase Now</a></li>
          </ul>
        </div>
      </div>

    </div>
  </div>
</div>

<div id="testimonials" class="testimonials">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div class="testimonials__carousel owl-carousel owl-theme">
          <div class="item">
            <div class="testimonials__content">
              <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam magna est, tempus ut facilisis sed, luctus eget erat. Suspendisse tellus neque, porta quis felis id, faucibus tempor diam. Donec sit amet purus et ipsum ultricies porta. "</p>
              <p class="testimonials__author">John Doe</p>
              <p class="testimonials__position">Designer</p>
            </div>
          </div>
          <div class="item">
            <div class="testimonials__content">
              <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam magna est, tempus ut facilisis sed, luctus eget erat. Suspendisse tellus neque, porta quis felis id, faucibus tempor diam. Donec sit amet purus et ipsum ultricies porta. "</p>
              <p class="testimonials__author">John Doe</p>
              <p class="testimonials__position">Designer</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>


<div id="contact" class="contact">
  <div class="container">
    <div class="row">
      <div class="feature__heading text-center col">
        <h3>Have a Question?</h3>
        <p>Please fill out the form below and we will get in touch with you as soon as possible. Thanks!</p>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-6">
        <p><span><i class="fas fa-mobile-alt"></i></span> 800-989-5690</p>
        <p><span><i class="far fa-envelope"></i></span> info@mydigitalmediakit.com</p>
        <p><span><i class="fas fa-map-marker-alt"></i></span> 520 N. Orlando Ave. #2, Winter Park, FL 32789</p>
        <form>
          <div class="form-input multi-line">
            <input type="text" placeholder="Name" name="" value="">
            <input type="text" placeholder="Email" name="" value="">
          </div>
          <div class="form-input">
            <textarea placeholder="Message" rows="5"></textarea>
          </div>
          <div class="form-submit">
            <input type="submit" name="" value="Send Message" class="btn btn__form">
          </div>
        </form>
      </div>
      <div class="col-lg-6">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3502.8936541038383!2d-81.36509798497474!3d28.602966992169378!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x88e7706b4724ae2d%3A0xd6df51703ca05f8!2sThe+Dicks+%2B+Nanton+Celebrity+Branding+Agency!5e0!3m2!1sen!2sus!4v1532343720308" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
      </div>
    </div>
  </div>
</div>

<?php get_footer(); ?>
