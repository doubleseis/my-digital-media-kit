<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
*/

// if ( ! defined( 'WP_CONTENT_DIR' ) ) {
//     define( 'WP_CONTENT_DIR', dirname( __FILE__ ) . '/wp-content' );
// }
// if ( ! defined( 'WP_CONTENT_URL' ) ) {
//     define( 'WP_CONTENT_URL', $_SERVER['HTTP_HOST'] . '/wp-content' );
// }

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'mydmk');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Qef3KdeD,$M|?]6z0=|JyNKPQRLb~-dL_Z-BLonD*TgqN&M4u~F)!k|BOtn={[i,');
define('SECURE_AUTH_KEY',  '5}I*.%_1-X<K(;nUNPX|=13wzYdUS_R|??Oa#AB|0l$hI&+-m[9ZY-Odc*M{T9)*');
define('LOGGED_IN_KEY',    '.z<;d&MiPt|}s;a,+BKckN<ye8OD[->`-Tx&j2x5p8|ek8Zo[|(C-1TSgnCicyfB');
define('NONCE_KEY',        ' 4x<UE%K*1yuhoDJ-:esU/}+Jssm}gVz<2x4s*8?`^O@Hu8ng>|r[<zp2|- Ql-2');
define('AUTH_SALT',        'VE]xo*boO30wVG)|;>i>O]5Vg(]uK7lZ3k)Rk}J83!r{e(H.phS;?ojJ=:fYk:eo');
define('SECURE_AUTH_SALT', '(FW!)=_|qj.[pyF2|8FG0?63g?=Q>)4;;w@*b-Nc~DM$?mlP{C-GzdJT[Wc%`WMZ');
define('LOGGED_IN_SALT',   '&&%S1K|A9427GoeHOORQOem)k_PJ?K@F/93eq|-<k{o^U8=M{Rx!Woz}Pl-S6-K~');
define('NONCE_SALT',       'cQUO+wa9B`|wFV93f.+[LSO>xmu++@|9uQV0R.~Uyn/1vk{#[5[`lIY}<Cq;d.;C');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
    define( 'ABSPATH', dirname( __FILE__ ) . '/wordpress/' );
}

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
